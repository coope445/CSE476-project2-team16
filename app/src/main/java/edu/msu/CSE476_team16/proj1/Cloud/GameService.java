package edu.msu.CSE476_team16.proj1.Cloud;

import edu.msu.CSE476_team16.proj1.Cloud.Models.Catalog;
import edu.msu.CSE476_team16.proj1.Cloud.Models.Connect4Result;
import edu.msu.CSE476_team16.proj1.Cloud.Models.DeleteResult;
import edu.msu.CSE476_team16.proj1.Cloud.Models.Item;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.LOGIN_URL;
import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.GAME_URL;
import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.DELETE_URL;
import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.LOAD_URL;
import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.CREATE_GAME_URL;
import static edu.msu.CSE476_team16.proj1.Cloud.Cloud.CREATE_USER_URL;

public interface GameService {
    @GET(LOGIN_URL)
    Call<Catalog> getCatalog(
            @Query("magic") String magic
    );

    @FormUrlEncoded
    @POST(GAME_URL)
    Call<DeleteResult> saveGame(
            @Field("user") String userId,
            @Field("magic") String magic,
            @Field("pw") String password,
            @Field("xml") String xmlData);

    @GET(DELETE_URL)
    Call<DeleteResult> deleteGame(
            @Query("user") String userId,
            @Query("magic") String magic,
            @Query("pw") String password
    );
    @GET(LOAD_URL)
    Call<DeleteResult> loadGameState(
            @Query("user") String userId,
            @Query("magic") String magic,
            @Query("pw") String password,
            @Query("id") String idToLoad
    );

    @GET(CREATE_USER_URL)
    Call<DeleteResult> createUser(
            @Query("user") String userId,
            @Query("magic") String magic,
            @Query("pw") String password
    );

    @GET(CREATE_GAME_URL)
    Call<Item> createGame(
            @Query("user") String userId,
            @Query("magic") String magic,
            @Query("pw") String password
    );

    @GET(CREATE_GAME_URL)
    Call<edu.msu.CSE476_team16.proj1.Cloud.Models.LoadResult> load_game(
            @Query("user") String userId,
            @Query("magic") String magic,
            @Query("pw") String password,
            @Query("id") String idHatToLoad
    );

    @GET(LOGIN_URL)
    Call<Connect4Result> loginGame(
            @Query("magic") String magic,
            @Query("user") String userId,
            @Query("pw") String password
    );

    @POST(CREATE_GAME_URL)
    Call<Connect4Result> updateGame(
            @Query("magic") String magic,
            @Query("user") String userId,
            @Query("pw") String password,
            @Query("game_id") String gameId,
            @Query("row_id") String rowId,
            @Query("col_id") String colId
    );

}
