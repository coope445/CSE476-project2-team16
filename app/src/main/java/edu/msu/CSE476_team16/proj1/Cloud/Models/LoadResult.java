package edu.msu.CSE476_team16.proj1.Cloud.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "connect4")
public class LoadResult {

    @Attribute(name = "status")
    private String status;

    @Attribute(name = "current_player_id")
    private String currentPlayerId;

    @ElementList(name = "column", inline = true, required = false)
    private List<Column> columns;

    public List<Column> getColumns() {
        return columns;
    }

    public String getCurrentPlayerId() {
        return currentPlayerId;
    }

    public String getStatus() {
        return status;
    }

}

@Root(name = "column", strict = false)
class Column {

    @Element(name = "connect4", required = false)
    private List<Row> rows;

    @Attribute(name = "column", required = false)
    private String column;

    public String getColumn() {
        return column;
    }

    public List<Row> getRows() {
        return rows;
    }
}

@Root(name = "connect4", strict = false)
class Row {

    @Attribute(name = "row", required = false)
    private String row;

    @Attribute(name = "value", required = false)
    private String value;

    public String getRow() {
        return row;
    }

    public String getValue() {
        return value;
    }
}
