package edu.msu.CSE476_team16.proj1;

import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import edu.msu.CSE476_team16.proj1.Cloud.GameService;
import edu.msu.CSE476_team16.proj1.Cloud.Models.Catalog;
import edu.msu.CSE476_team16.proj1.Cloud.Models.Connect4Result;
import edu.msu.CSE476_team16.proj1.Cloud.Models.Item;
import edu.msu.CSE476_team16.proj1.Cloud.Models.LoadResult;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class Cloud3 {

    public static final String MAGIC = "uAss+5%FP'hK&65";

    private static final String BASE_URL = "https://webdev.cse.msu.edu/~samoyhun/cse476/project2/";
    private static final String LOGIN_URL = "https://webdev.cse.msu.edu/~samoyhun/cse476/project2/login.php";
    private static final String CREATE_URL = "https://webdev.cse.msu.edu/~samoyhun/cse476/project2/create-user.php";

    private static final String UTF8 = "UTF-8";

    private String theUsername = "";
    private String thePassword = "";

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build();

    public static class CatalogAdapter extends BaseAdapter {

        /**
         * The items we display in the list box. Initially this is
         * null until we get items from the server.
         */
        private Catalog catalog = new Catalog("", new ArrayList<Item>(), "");

        //private Retrofit retrofit = null;

        /**
         * Constructor
         */
        public CatalogAdapter(final View view) {
            // Create a thread to load the catalog
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        catalog = getCatalog();

                        if (catalog.getStatus().equals("no")) {
                            String msg = "Loading catalog returned status 'no'! Message is = '" + catalog.getMessage() + "'";
                            throw new Exception(msg);
                        }
                        if (catalog.getItems().isEmpty()) {
                            String msg = "Catalog does not contain any games.";
                            throw new Exception(msg);
                        }
                        view.post(new Runnable() {

                            @Override
                            public void run() {
                                // Tell the adapter the data set has been changed
                                notifyDataSetChanged();
                            }

                        });
                    } catch (final Exception e) {
                        // Error condition! Something went wrong
                        Log.e("CatalogAdapter", "Something went wrong when loading the catalog", e);
                        view.post(new Runnable() {
                            @Override
                            public void run() {
                                String string;
                                // make sure that there is a message in the catalog
                                // if there isn't use the message from the exception
                                if (catalog.getMessage() == null) {
                                    string = e.getMessage();
                                } else {
                                    string = catalog.getMessage();
                                }
                                Toast.makeText(view.getContext(), string, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }


        public Catalog getCatalog() throws IOException, RuntimeException {
            GameService service = retrofit.create(GameService.class);
            //return service.getCatalog(USER, MAGIC, PASSWORD).execute().body();
            Response<Catalog> response = service.getCatalog(MAGIC).execute();
            // check if request failed
            if (!response.isSuccessful()) {
                Log.e("getCatalog", "Failed to get catalog, response code is = " + response.code());
                return new Catalog("no", new ArrayList<Item>(), "Server error " + response.code());
            }
            Catalog catalog = response.body();
            if (catalog.getStatus().equals("no")) {
                String string = "Failed to get catalog, msg is = " + catalog.getMessage();
                Log.e("getCatalog", string);
                return new Catalog("no", new ArrayList<Item>(), string);
            };
            if (catalog.getItems() == null) {
                catalog.setItems(new ArrayList<Item>());
            }
            return catalog;
        }

        @Override
        public int getCount() {
            return catalog.getItems().size();
        }

        @Override
        public Item getItem(int position) {
            return catalog.getItems().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            //if(view == null) {
            //view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_item, parent, false);
            //}

            //TextView tv = (TextView)view.findViewById(R.id.textItem);
            //tv.setText(catalog.getItems().get(position).getName());
            return view;
        }

        public String getId(int position) {
            return catalog.getItems().get(position).getId();
        }
        public String getName(int position) {
            return catalog.getItems().get(position).getName();
        }
    }

    /**
     * Open a connection to a game in the cloud.
     *
     * @param id id for the game
     * @return reference to an input stream or null if this fails
     */
    public LoadResult openFromCloud(final String id) {

        GameService service = retrofit.create(GameService.class);
        try {
            Response<edu.msu.CSE476_team16.proj1.Cloud.Models.LoadResult> response = service.load_game(MAGIC, MAGIC, MAGIC, id).execute();

            // check if request failed
            if (!response.isSuccessful()) {
                Log.e("OpenFromCloud", "Failed to load game, response code is = " + response.code());
                return null;
            }

            edu.msu.CSE476_team16.proj1.Cloud.Models.LoadResult result = response.body();
            if (result.getStatus().equals("yes")) {
                return result;
            }

            Log.e("OpenFromCloud", "Failed to load game, message is" );
            return null;
        } catch (IOException | RuntimeException e) {
            Log.e("OpenFromCloud", "Exception occurred while loading game", e);
            return null;
        }
    }

    /**
     * Save a hatting to the cloud.
     * This should be run in a thread.
     * @param name name to save under
     * @param view view we are getting the data from
     * @return true if successful
     */
    public boolean saveToCloud(String name, GameBoardView view) throws IOException {

        name = name.trim();
        if(name.length() == 0) {
            return false;
        }
        // Create an XML packet with the information about the current image
        XmlSerializer xml = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try {
            xml.setOutput(writer);

            xml.startDocument("UTF-8", true);

            //view.saveXml(name, xml); Assume empty board

            xml.endDocument();

        } catch (IOException e) {
            // This won't occur when writing to a string
            return false;
        }

        GameService service = retrofit.create(GameService.class);
        try {
            String xmlString = writer.toString();
            Response<Item> response = service.createGame(theUsername, MAGIC, thePassword).execute();
            if (response.isSuccessful()) {
                Item result = response.body();
                boolean check = false;
                if(result==null){
                    check=false;
                }

                if(check) {
                    if (result.getMessage().equals("login successful")) {
                        return true;
                    }
                }
                Log.e("SaveToCloud", "Failed to save, message = '" + result.getMessage() + "'");
                return false;
            }
            Log.e("SaveToCloud", "Failed to save, message = '" + response.code() + "'");
            //Log.e("SaveToCloud", rtn.errorBody().string() );
            return false;
        } catch (IOException e) {
            Log.e("SaveToCloud", "Exception occurred while trying to save hat!", e);
            return false;
        } catch (RuntimeException e) {
            Log.e("SaveToCloud", "Runtime exception: " + e.getMessage());
            //Log.e("SaveToCloud", "XML Error: " + rtn.errorBody() + " " + rtn.errorBody().string());
            return false;
        }
    }

    public boolean createAccount(String username, String password) {
        theUsername = username;
        thePassword = password;
        String query = CREATE_URL + "?user=" + username + "&magic=" + MAGIC + "&password=" + password;

        try {
            URL url = new URL(query);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            int responseCode = conn.getResponseCode();
            if(responseCode != HttpURLConnection.HTTP_OK) {
                return false;
            }

            InputStream stream = conn.getInputStream();

            /**
             * Create an XML parser for the result
             */
            try {
                XmlPullParser xmlR = Xml.newPullParser();
                xmlR.setInput(stream, UTF8);

                xmlR.nextTag();      // Advance to first tag
                xmlR.require(XmlPullParser.START_TAG, null, "toucan");

                String status = xmlR.getAttributeValue(null, "status");
                if(status.equals("no")) {
                    return false;
                }

                // We are done
            } catch(XmlPullParserException ex) {
                return false;
            } catch(IOException ex) {
                return false;
            }
            stream.close();

        } catch (MalformedURLException e) {
            // Should never happen
            return false;
        } catch (IOException ex) {
            return false;
        }

        return false;
    }

    public Connect4Result loginFunction(final String username, final String password) {
        theUsername = username;
        thePassword = password;

        GameService service = retrofit.create(GameService.class);
        try {
            // Attempt to log in on the server
            Response<Connect4Result> response = service.loginGame(MAGIC, username, password).execute();

            // If the server could not be reached, return null
            if (!response.isSuccessful()) {
                return null;
            }

            Connect4Result result=response.body();
            if (result.getStatus().equals("yes")) {
                return result;
            }


            // Return the result
            return response.body();
        } catch (IOException | RuntimeException e) {
            // There was another problem with processing the data from the database, return null
            Log.e("MSG",e.getMessage());
            return null;
        }

    }

    public Connect4Result updateGame(final String username, final String password, final String gameId, final String rowId, final String colId) {


        GameService service = retrofit.create(GameService.class);
        try {
            // Attempt to log in on the server
            Response<Connect4Result> response = service.updateGame(MAGIC, username, password,gameId,rowId,colId).execute();

            // If the server could not be reached, return null
            if (!response.isSuccessful()) {
                return null;
            }

            Connect4Result result=response.body();
            if (result.getStatus().equals("yes")) {
                return result;
            }


            // Return the result
            return response.body();
        } catch (IOException | RuntimeException e) {
            // There was another problem with processing the data from the database, return null
            Log.e("MSG",e.getMessage());
            return null;
        }

    }

}



