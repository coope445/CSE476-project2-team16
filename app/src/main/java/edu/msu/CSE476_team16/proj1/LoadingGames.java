package edu.msu.CSE476_team16.proj1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import edu.msu.CSE476_team16.proj1.Cloud.Models.LoadResult;

import androidx.fragment.app.DialogFragment;
public class LoadingGames extends DialogFragment {
    /**
     * Id for the image we are loading
     */
    private String catId;

    /**
     * Create the dialog box
     */

    /**
     * Set true if we want to cancel
     */
    private volatile boolean cancel = false;

    private final static String ID = "id";

    @Override
    public Dialog onCreateDialog(Bundle bundle) {

        if(bundle != null) {
            catId = bundle.getString(ID);
        }

        cancel = false;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set the title
        builder.setTitle(R.string.loading);

        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        cancel = true;
                    }
                });


        // Create the dialog box
        final AlertDialog dlg = builder.create();

        // Get a reference to the view we are going to load into
        final GameBoardView view = (GameBoardView)getActivity().findViewById(R.id.gameView);

        /*
         * Create a thread to load the game from the cloud
         */
        new Thread(new Runnable() {

            @Override
            public void run() {
                // Create a cloud object and get the XML
                Cloud3 cloud = new Cloud3();
                LoadResult game = cloud.openFromCloud(catId);

                if (cancel) {
                    return;
                }

                // game loaded successfully
                if (game != null) {
                    view.loadGame(game);
                    view.post(new Runnable() {
                        @Override
                        public void run() {
                            dlg.dismiss();
                            // Success!
                            if (getActivity() instanceof GameBoardActivity) {
                                ((GameBoardActivity) getActivity()).updateBoard();
                            }
                        }
                    });
                    return;
                }

                view.post(new Runnable() {
                    @Override
                    public void run() {
                        dlg.dismiss();
                        Toast.makeText(view.getContext(),
                                R.string.loading_fail,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
        return dlg;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    /**
     * Save the instance state
     */
    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);

        bundle.putString(ID, catId);
    }

    /**
     * Called when the view is destroyed.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cancel = true;
    }
}
