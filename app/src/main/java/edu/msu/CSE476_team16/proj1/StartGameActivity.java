package edu.msu.CSE476_team16.proj1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class StartGameActivity extends AppCompatActivity {

    private final static String GREEN_NAME = "MainActivity.nameOfGreen";
    private final static String WHITE_NAME = "MainActivity.nameOfWhite";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

    }

    /**
     * Handles the create game button
     * @param view the view
     */
    public void onCreateGame(View view) {
        //TextView playerOneText = (TextView)this.findViewById(R.id.usernameEditor);
        //TextView playerTwoText = (TextView)this.findViewById(R.id.passwordEditor);

        /*
        Send to server that game was made with this user as the player
        Pass name
         */


        //starts the GameBoardActivity
        Intent intent = new Intent(this, GameBoardActivity.class);
        //intent.putExtra(GREEN_NAME, playerOneText.getText().toString());
        //intent.putExtra(WHITE_NAME, playerTwoText.getText().toString());
        intent.putExtra(GREEN_NAME, "Javier");
        intent.putExtra(WHITE_NAME, "Jamie");
        startActivity(intent);
    }

    /**
     * Handles the join game button
     * @param view the view
     */
    public void onJoinGame(View view){
        /*
        No menu popup is necessary, should ask the cloud if a game exists, if it does then join the game
        and start it. Should also activate GameBoardActivity
         */
        LoadGames dlg2 = new LoadGames();
        dlg2.show(getSupportFragmentManager(), "load");
    }
}