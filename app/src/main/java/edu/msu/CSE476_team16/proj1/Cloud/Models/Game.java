package edu.msu.CSE476_team16.proj1.Cloud.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "connect4")
public class Game {
    @Attribute
    private String name;

    @Attribute
    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public Game() {}

    public Game(Integer id, String name, String uri) {
        this.name = name;
        this.uri = uri;
    }
}
